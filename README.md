# CPT202
_大家好，我是傻逼杨子川_

## 1. 如何使用
### 关于 GitLab
idea 选手参考这篇[文章](https://blog.csdn.net/qq_41806718/article/details/106333053)使用 GitLab 插件
### 关于 MySQL
请新建一个名为 `cpt202` 的数据库，用户名为 `root`，密码为 `password`（参考 application.properties 文件）

如果使用 MySQL 时出现连接不到 localhost 的情况，请参考这篇[文章](https://blog.csdn.net/epyingxue/article/details/86085942)

## 2. 使用时注意
现在有两个分支 `main` 和 `master`，我们项目的主分支是 `main`，不要往 `master` 分支合并。在最后要把 `master` 分支删掉的。

## 3. 一些文件
- `/main/templates` 下创建了两个个文件夹 `/admin`、`/user`，分别用来放 **只有管理员或者只有客户能看见的页面** ，管理员和客户都能看见的页面直接放在 `/templates` 目录下。跳转该网页时可以通过如下方式：
    ```java
    // 例如要跳转到 /user/page.html 这个网页
    @GetMapping("/page")
    public String MaintainPage() {
        return "user/page";
    }
    ```
- home page 分了两个：`HomePage.html` 和 `/admin/AdminHomePage.html`，因为管理员的主页应该会和普通用户的不一样（大概
- java 文件的存放路径按 pbi 编号来创建，如杨子川的第一个 pbi 是 `MSUS01`，则路径为 `/java/com.CPT202.PetGroomingSystem/MS/US/`

大家也可以把自己创建了什么文件夹、用来做什么都往这里写一下，省的乱 :)
