package com.CPT202.PetGroomingSystem.MS.US.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Discount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String info;
    private String cond;
    private float price;
    private String timelmt;

    public Discount() { }
    public Discount(int id, String info, String cond, float price, String timelmt) {
        this.id = id;
        this.info = info;
        this.cond = cond;
        this.price = price;
        this.timelmt = timelmt;
    }

    public float getPrice() { return this.price; }
    public void setPrice(float price) { this.price = price; }
    public int getId() { return this.id; }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimelmt() {
        return timelmt;
    }

    public String getCond() {
        return cond;
    }

    public String getInfo() {
        return info;
    }

    public void setCond(String cond) {
        this.cond = cond;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setTimelmt(String timelmt) {
        this.timelmt = timelmt;
    }
}
