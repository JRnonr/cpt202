package com.CPT202.PetGroomingSystem.MS.US.Repo;

import com.CPT202.PetGroomingSystem.MS.US.models.Discount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DiscountRepo extends JpaRepository<Discount, Integer> {
}
