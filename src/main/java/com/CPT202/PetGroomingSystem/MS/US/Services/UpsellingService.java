package com.CPT202.PetGroomingSystem.MS.US.Services;

import com.CPT202.PetGroomingSystem.MS.US.Controllers.UpsellingController;
import com.CPT202.PetGroomingSystem.MS.US.Repo.UpsellingRepo;
import com.CPT202.PetGroomingSystem.MS.US.models.Discount;
import com.CPT202.PetGroomingSystem.MS.US.models.Upselling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UpsellingService {
    @Autowired
    private UpsellingRepo upsellingRepo;

    public Upselling newUpselling(Upselling s) {
        if (Objects.equals(s.getCond(), "") || Objects.equals(s.getInfo(), "") || Objects.equals(s.getTimelmt(), "") || Objects.equals(s.getCont(), ""))
            return null;
        return upsellingRepo.save(s);
    }

    public List<Upselling> getList() { return upsellingRepo.findAll(); }
    public List<Upselling> getDescList() {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        return upsellingRepo.findAll(sort);
    }

    public void delUpselling(Upselling s) {
        upsellingRepo.delete(s);
    }
    public Upselling findById(int id) {
        List<Upselling> UpList = upsellingRepo.findAll();
        for (Upselling i : UpList) {
            if (i.getId() == id) return i;
        }
        return null;
    }
}
