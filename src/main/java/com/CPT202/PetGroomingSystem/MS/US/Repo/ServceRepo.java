package com.CPT202.PetGroomingSystem.MS.US.Repo;

import com.CPT202.PetGroomingSystem.MS.US.models.Servce;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServceRepo extends JpaRepository<Servce, Integer> {
}
