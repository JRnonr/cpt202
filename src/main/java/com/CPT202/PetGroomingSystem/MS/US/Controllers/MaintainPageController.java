package com.CPT202.PetGroomingSystem.MS.US.Controllers;

import com.CPT202.PetGroomingSystem.MS.US.Services.DiscountService;
import com.CPT202.PetGroomingSystem.MS.US.Services.UpsellingService;
import com.CPT202.PetGroomingSystem.MS.US.models.Discount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MaintainPageController extends rootController {
    @GetMapping("/MaintainServicesPage")
    public String maintainPage(Model model) {
        return backMaintainPage(model);
    }
}
