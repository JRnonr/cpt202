package com.CPT202.PetGroomingSystem.MS.US.Controllers;

import com.CPT202.PetGroomingSystem.MS.US.Services.DiscountService;
import com.CPT202.PetGroomingSystem.MS.US.Services.ServceService;
import com.CPT202.PetGroomingSystem.MS.US.Services.UpsellingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

@Controller
public class rootController {
    @Autowired
    private UpsellingService upsellingService;
    @Autowired
    private DiscountService discountService;
    @Autowired
    private ServceService servceService;

    public String backMaintainPage(Model model) {
        model.addAttribute("DiscountList", discountService.getDescList());
        model.addAttribute("UpsellList", upsellingService.getDescList());
        model.addAttribute("ServceList", servceService.getDescList());
        return "admin/MaintainServicesPage";
    }
}
